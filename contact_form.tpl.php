<!-- More Contacts Information -->
<div class="background">
    <div class="morecontacts">
        <h3><span class="maintext"><?php print variable_get('contact_form_title'); ?></span></h3>
        <ul class="contacts">
            <li><i class="fa fa-map-marker"></i><?php print variable_get('contact_form_street'); ?></li>
            <li><i class="fa fa-envelope" style="left: -3px;"></i><a href="mailto:<?php print variable_get('contact_form_email'); ?>"><?php print variable_get('contact_form_email'); ?></a></li>
   
            <?php if(variable_get('contact_form_phone')): ?>          
            	<li><i class="fa fa-phone"></i><?php print variable_get('contact_form_phone'); ?></li>
            <?php endif; ?> 
       
            <?php if(variable_get('contact_form_whatsapp')): ?>                        
           		<li><i class="fa fa-print"></i><?php print variable_get('contact_form_whatsapp'); ?></li>
            <?php endif; ?> 
        </ul>
    </div>
</div>

